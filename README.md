# README #

Repo para generar el contenedor docker de PostgreSQL para el proyecto de la Tech University

### How do I get set up? ###

Para crear el contenedor:
> docker build -t <usuarioDocker>/<nombreImagen> .

Para ejecutarlo, se recomienda crear primero una red en docker. Una vez hecho:
>  sudo docker run -p 5433:5432 -v pgdata:/ver/lib/postgresql/9.3/main --net redtechu --name serverpostgresql  <usuarioDocker>/<nombreImagen>

Una vez lo hemos hecho una vez, podemos volver a ejecutar la imagen con:
> docker start serverpostgresql

Para pararlo:
> docker stop serverpostgresql

### Who do I talk to? ###

* Pablo Bezon